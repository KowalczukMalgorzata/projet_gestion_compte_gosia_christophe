<?php

while (true) {
    $cptCompte = 0;
    $cptClient = 0;
    echo ("Identifiant(s) disponible(s) : " . PHP_EOL);
    foreach ($clients as $keys => $client) {
        foreach ($client as $key => $value) {
            if ($key == "code") {
                echo ("Nom : " . $client["nom"] . " => identifiant : " . $value . PHP_EOL);
            }
        }
    }
    $clientRecherche = readline("Saisir l'identifiant du client à afficher : ");
    foreach ($clients as $client) {
        if ($clientRecherche != $client["code"]) {
            $cptClient++;
        }
    }
    if ($cptClient == count($clients)) {
        echo ("Identifiant inconnu" . PHP_EOL);
        break;
    }

    while (true) {
        foreach ($comptes as $keys => $compte) {
            foreach ($compte as $key => $value) {
                if ($clientRecherche == $value) {
                    $codeClient = $compte["codeClient"];
                    $cptCompte++;
                    // break 3;
                }
            }
        }
        
        break;
    }

    if ($cptCompte == 0) {
        change_color("red");
        readline("Ce client ne posséde pas de compte");
        change_color("");
    }


    if (isset($codeClient)) {
        while (true) {
            foreach ($clients as $cles => $client) {
                foreach ($client as $cle => $val) {
                    if ($cle == "code" && $val == $codeClient) {
                        break 3;
                    }
                }
            }
        }
    }


    $afficherFicheClient = ("../v2/impression/Infosclient.txt");
    $f = fopen($afficherFicheClient, 'w');

    $ficheClient = ("                      Fiche client" . PHP_EOL . PHP_EOL .
        "Numéro client : " . $client["code"] . PHP_EOL .
        "Nom : " . $client["nom"] . PHP_EOL .
        "Prénom : " . $client["prenom"] . PHP_EOL .
        "Date de naissance : " . $client["dateDeNaissance"] . PHP_EOL . PHP_EOL .
        "_____________________________________________________________________" . PHP_EOL .
        "Liste de compte" . PHP_EOL .
        "_____________________________________________________________________" . PHP_EOL .
        "Numéro de compte                  Solde" . PHP_EOL .
        "_____________________________________________________________________" . PHP_EOL . PHP_EOL);


    fwrite($f, $ficheClient);
    if (isset($codeClient)) {
        while (true) {
            foreach ($comptes as $cles => $compte) {
                foreach ($compte as $cle => $val) {
                    if ($cle == "codeClient" && $val == $codeClient) {
                        if (!empty($compte["solde"]) && $compte["solde"] >= 0) {
                            $courant = ("    " . $compte["idCompte"] . "                   " . $compte["solde"] . "                   :-)" . PHP_EOL);
                            fwrite($f, $courant);
                        } elseif (!empty($compte["solde"]) && $compte["solde"] <= 0) {
                            $courant = ("    " . $compte["idCompte"] . "                   " . $compte["solde"] . "                   :-(" . PHP_EOL);
                            fwrite($f, $courant);
                        }
                        if (!empty($compte["soldeLivretA"]) && $compte["soldeLivretA"] >= 0) {
                            $livretA = ("    " . $compte["idCompte"] . "                   " . $compte["soldeLivretA"] . "                   :-)" . PHP_EOL);
                            fwrite($f, $livretA);
                        } elseif (!empty($compte["soldeLivretA"]) && $compte["soldeLivretA"] <= 0) {
                            $livretA = ("    " . $compte["idCompte"] . "                   " . $compte["soldeLivretA"] . "                   :-(" . PHP_EOL);
                            fwrite($f, $livretA);
                        }
                        if (!empty($compte["soldePEL"]) && $compte["soldePEL"] >= 0) {
                            $pel = ("    " . $compte["idCompte"] . "                   " . $compte["soldePEL"] . "                   :-)" . PHP_EOL);
                            fwrite($f, $pel);
                        }
                        if (!empty($compte["soldePEL"]) && $compte["soldePEL"] <= 0) {
                            $pel = ("    " . $compte["idCompte"] . "                   " . $compte["soldePEL"] . "                   :-(" . PHP_EOL);
                            fwrite($f, $pel);
                        }
                    }
                }
            }
            break;
        }

        fclose($f);
        break;
    }
}
