<?php

while (true) {
    $trouve = 0;
    echo ("Identifiant(s) disponible(s) : " . PHP_EOL);
    foreach ($clients as $keys => $client) {
        foreach ($client as $key => $value) {
            if ($key == "code") {
                echo ("Nom : " . $client["nom"] . PHP_EOL .
                    "identifiant : " . $value . PHP_EOL . PHP_EOL);
            }
        }
    }
    $clientRecherche = readline("Saisir l'identifiant du client à afficher : ");
    while ($clientRecherche == "") {
        change_color(("red"));
        $clientRecherche = readline("Invalide! Veuillez Saisir l'identifiant client recherché : ");
        change_color("");
    }
    while (true) {
        foreach ($comptes as $keys => $compte) {
            foreach ($compte as $key => $value) {
                if ($clientRecherche == $value) {
                    $codeClient = $compte["codeClient"];
                    $trouve = 1;
                    break 3;
                }
            }
        }
        if ($trouve != 1) {
            change_color("red");
            readline("Ce client n'existe pas ou ne possède pas encore de compte ! appuyer sur une touche pour retourner au menu principal : ");
            change_color("");
            $trouve = 0;
            break;
        }
    }

    if (isset($codeClient)) {
        while (true) {
            foreach ($clients as $cles => $client) {
                foreach ($client as $cle => $val) {
                    if ($cle == "code" && $val == $codeClient) {
                        break 3;
                    }
                }
            }
        }
    } else {
        break;
    }

    change_color("blue");
    echo (PHP_EOL . PHP_EOL .
        "Numéro client : " . $client["code"] . PHP_EOL .
        "Nom : " . $client["nom"] . PHP_EOL .
        "Prénom : " . $client["prenom"] . PHP_EOL .
        "Mail : " . $client["mail"] . PHP_EOL .
        "Date de naissance : " . $client["dateDeNaissance"] . PHP_EOL . PHP_EOL .
        "_______________________" . PHP_EOL .
        "Liste de(s) compte(s) :" . PHP_EOL . PHP_EOL);
    change_color("");

    if (isset($codeClient)) {
        $compteTrouve = 0;
        while (true) {
            foreach ($comptes as $cles => $compte) {
                foreach ($compte as $cle => $val) {
                    if ($cle == "codeClient" && $val == $codeClient) {
                        if (isset($compte["solde"]) && $compte["solde"] >= 0 && $compte["solde"] != "") {
                            change_color("green");
                            echo ("Compte courant numéro : " . $compte["idCompte"] . PHP_EOL . PHP_EOL);
                            change_color("");
                            $compteTrouve++;
                        } elseif (isset($compte["solde"]) && $compte["solde"] <= 0 && $compte["solde"] != "") {
                            change_color("red");
                            echo ("Compte courant numéro : " . $compte["idCompte"] . PHP_EOL . PHP_EOL);
                            change_color("");
                            $compteTrouve++;
                        }
                        if (isset($compte["soldeLivretA"]) && $compte["soldeLivretA"] >= 0 && $compte["soldeLivretA"] != "") {
                            change_color("green");
                            echo ("Livret A numéro : " . $compte["idCompte"] . PHP_EOL . PHP_EOL);
                            change_color("");
                            $compteTrouve++;
                        } elseif (isset($compte["soldeLivretA"]) && $compte["soldeLivretA"] <= 0 && $compte["soldeLivretA"] != "") {
                            change_color("red");
                            echo ("Livret A numéro : " . $compte["idCompte"] . PHP_EOL . PHP_EOL);
                            change_color("");
                            $compteTrouve++;
                        }
                        if (isset($compte["soldePEL"]) && $compte["soldePEL"] >= 0 && $compte["soldePEL"] != "") {
                            change_color("green");
                            echo ("Compte épargne logement numéro : " . $compte["idCompte"] . PHP_EOL . PHP_EOL);
                            change_color("");
                            $compteTrouve++;
                        }
                        if (isset($compte["soldePEL"]) && $compte["soldePEL"] <= 0 && $compte["soldePEL"] != "") {
                            change_color("red");
                            echo ("Compte épargne logement numéro : " . $compte["idCompte"] . PHP_EOL . PHP_EOL);
                            change_color("");
                            $compteTrouve++;
                        }
                    } 
                }
            }
            if ($compteTrouve == 0) {
                change_color("red");
                echo("Aucun compte enregistré pour ce client" . PHP_EOL . PHP_EOL);
                change_color("");
            break;
            }
            break;
        }
        break;
    } 
    echo (PHP_EOL . PHP_EOL);
}
