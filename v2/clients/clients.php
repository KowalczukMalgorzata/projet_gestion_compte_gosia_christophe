<?php

while (true) {
    if (!isset($clients)) {
        $clients = [];
    }
    $idClient = strtoupper(substr(str_shuffle("abcdefghijklmnopqrstuvwxyz"), 0, 2)) . random_int(100000, 999999);
    foreach ($clients as $elements) {
        foreach ($elements as $key => $value) {
            if ($key == "code") {
                while ($idClient == $value) {
                    $idClient = strtoupper(substr(str_shuffle("abcdefghijklmnopqrstuvwxyz"), 0, 2)) . random_int(100000, 999999);
                }
            }
        }
    }

    $nomClient = readline("Entrer le nom du client : ");

    while (!preg_match("/[a-zA-Z][\p{L}-]*$/", $nomClient)) {
        change_color("red");
        echo ("Nom invalide !" . PHP_EOL);
        change_color("");
        $nomClient = readline("Entrer le nom du client : ");
    }

    $bis = 2;
    foreach ($clients as $keys => $elements) {
        foreach ($elements as $key => $value) {
            if ($key == "nom") {
                while ($nomClient == $value) {
                    change_color("red");
                    echo ("Une entrée proche a été trouvée : " . PHP_EOL . PHP_EOL);
                    change_color("");
                    change_color("purple");
                    echo ("    Client " . $elements["nom"] . PHP_EOL .
                        "    ID : " . $elements["code"] . PHP_EOL .
                        "    Mail : " . $elements["mail"] . PHP_EOL . PHP_EOL);
                    change_color("");
                    $reponse = strtoupper(readline("Voulez-vous continuer (O)ui / (N)on ? "));
                    while ($reponse != "O" && $reponse != "N") {
                        change_color("red");
                        $reponse = strtoupper(readline("Réponse invalide, taper (O)ui / (N)on : "));
                        change_color("");
                    }
                    if ($reponse == "O") {
                        $nomClient = $nomClient . "(" . $bis . ")";
                        $bis++;
                        break 3;
                    } elseif ($reponse == "N") {
                        break 4;
                    }
                }
            }
        }
    }

    $prenomClient = readline("Entrer le prenom : ");

    while (!preg_match("/[a-zA-Z][\p{L}-]*$/", $prenomClient)) {
        change_color("red");
        echo ("Prénom invalide !" . PHP_EOL);
        change_color("");
        $prenomClient = readline("Entrer le prénom du client : ");
    }

    $jour = (int)readline("entrer le jour de naissance : ");
    change_color("red");
    while (!is_numeric($jour) || $jour < 1 || $jour > 31) {
        $jour = (int)readline("entrer 1 jour de naissance entre 1 et 31 : ");
    }
    change_color("");
    $mois = (int)readline("entrer le mois de naissance : ");
    change_color("red");
    while (!is_numeric($mois) || $mois < 1 || $mois > 12) {
        $mois = (int)readline("entrer 1 mois de naissance entre 1 et 12 : ");
    }
    change_color("");
    $annee = (int)readline("entrer l'année de naissance : ");
    change_color("red");
    while (!is_numeric($annee) || $annee < (date("Y") - 100) || $annee > date("Y") - 18) {
        $annee = (int)readline("entrer 1 année de naissance valide (+18ans) : ");
    }
    change_color("");

    while (!checkdate($mois, $jour, $annee) || $annee > 2002 || $annee < 1900) {
        change_color("red");
        echo ("Date de naissance invalide ! (+18ans)" . PHP_EOL);
        change_color("");
        $jour = (int)readline("entrer le jour de naissance : ");
        change_color("red");
        while (!is_numeric($jour) || $jour < 1 || $jour > 31) {
            $jour = (int)readline("entrer 1 jour de naissance entre 1 et 31 : ");
        }
        change_color("");
        $mois = (int)readline("entrer le mois de naissance : ");
        change_color("red");
        while (!is_numeric($mois) || $mois < 1 || $mois > 12) {
            $mois = (int)readline("entrer 1 mois de naissance entre 1 et 12 : ");
        }
        change_color("");
        $annee = (int)readline("entrer l'année de naissance : ");
        change_color("red");
        while (!is_numeric($annee) || $annee < (date("Y") - 100) || $annee > date("Y") - 18) {
            $annee = (int)readline("entrer 1 année de naissance valide (+18ans) : ");
        }
        change_color("");
    }

    $DateDeNaissanceClient = $jour . "/" . $mois . "/" . $annee;

    $EmailClient = readline("Entrez l'email : ");
    while (!filter_var($EmailClient, FILTER_VALIDATE_EMAIL)) {
        change_color("red");
        $EmailClient = readline("Email invalide, Entrez l'email ( --- @ --- . com, fr ): ");
        change_color("");
    }
    foreach ($clients as $keys => $client) {
        foreach ($client as $key => $value) {
            if ($key == "mail") {
                while ($EmailClient == $value) {
                    change_color("red");
                    echo ("L'adresse email " . $EmailClient . " existe déjà : " . PHP_EOL . PHP_EOL);
                    change_color("");
                    change_color("purple");
                    echo ("    Client " . $elements["nom"] . PHP_EOL .
                        "    ID : " . $elements["code"] . PHP_EOL .
                        "    Mail : " . $elements["mail"] . PHP_EOL . PHP_EOL);
                    change_color("");
                    $reponse = strtoupper(readline("Voulez-vous continuer ? (O)ui / (N)on) : "));
                    while ($reponse != "O" && $reponse != "N") {
                        change_color("red");
                        $reponse = strtoupper(readline("Réponse invalide, taper (O)ui / (N)on : "));
                        change_color("red");
                    }
                    if ($reponse == "O") {
                        $client["mail"] = $EmailClient;
                        break 3;
                    } elseif ($reponse == "N") {
                        break 4;
                    }
                }
            }
        }
    }
    
    $client["code"] = $idClient;
    $client["nom"] = $nomClient;
    $client["prenom"] = $prenomClient;
    $client["dateDeNaissance"] = $DateDeNaissanceClient;
    $client["mail"] = $EmailClient;
    change_color("purple");
    echo ("Le client n° " . $idClient . " a bien été créé" . PHP_EOL);
    change_color("");
    $clients[] = $client;
    arrayToCsv($filename = '../v2/clients/clients.csv', $delimiter = ',', $clients, $header = array("code","nom","prenom","dateDeNaissance","mail"));
    $newClient = strtoupper(readline("Voulez-vous créer un autre client ? (O)ui / (N)on : "));
    while ($newClient != "O" && $newClient != "N") {
        change_color("red");
        $newClient = strtoupper(readline("Réponse invalide, taper (O)ui / (N)on : "));
        change_color("red");
    }
    if ($newClient == "O") {
        continue;
    }
    break;
}
