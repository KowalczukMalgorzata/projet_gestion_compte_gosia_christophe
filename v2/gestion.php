<?php

// fichier principal pour la gestion des comptes banquaires

// fichier utils 

include "../v2/lib/utils.php";

// fonctions
include "../v2/lib/function.php";

// initialisation

csv_to_array($filename = '../v2/agences/agences.csv', $delimiter = ',', $agences);
csv_to_array($filename = '../v2/clients/clients.csv', $delimiter = ',', $clients);
csv_to_array($filename = '../v2/comptes/comptes.csv', $delimiter = ',', $comptes);


while (true) {

    // Menu Principal pour le programme de gestion de comptes bancaires

    change_color("blue");
    echo (PHP_EOL . "Menu :" . PHP_EOL . PHP_EOL);
    change_color("");
    change_color("purple");
    echo ("   ---------------------- DWWM_20044 Banque ----------------------------" . PHP_EOL .
        "   1- Créer une agence" . PHP_EOL .
        "   2- créer un client" . PHP_EOL .
        "   3- Créer un compte bancaire" . PHP_EOL .
        "   4- Recherche de compte (numéro de compte)" . PHP_EOL .
        "   5- Recherche de client (Nom, Numéro de compte, identifiant de client)" . PHP_EOL .
        "   6- Afficher la liste des comptes d’un client (identifiant client)" . PHP_EOL .
        "   7- Imprimer les infos client (identifiant client)" . PHP_EOL .
        "   8- Simuler frais de gestion" . PHP_EOL .
        "   ---------------------------------------------------------------------" . PHP_EOL .
        "   9- Quitter le programme" . PHP_EOL .
        "   ---------------------------------------------------------------------" . PHP_EOL . PHP_EOL);
    change_color("");
    change_color("blue");
    $choixMenu = (int)(readline("Votre choix : "));
    change_color("");
    echo (PHP_EOL);
    while (true) {
        if (!is_numeric($choixMenu) || $choixMenu < 1 || $choixMenu > 9) {
            change_color("red");
            $choixMenu = (int)(readline("choix  invalide : "));
            change_color("");
        } else {
            break;
        }
    }

    if ($choixMenu == 9) {

        break;
    } elseif ($choixMenu == 1) {

        // Menu 1 : Créer une agence
        
        include "../v2/agences/agences.php";


    } elseif ($choixMenu == 2) {

        // Menu 2 : Créer un client
        include "../v2/clients/clients.php";
    } elseif ($choixMenu == 3) {

        // Menu 3 : Créer un compte bancaire
        include "../v2/comptes/comptes.php";
    } elseif ($choixMenu == 4) {

        // Menu 4 : Recherche de compte (numéro de compte)
        include "../v2/comptes/rechercheCompte.php";
    } elseif ($choixMenu == 5) {

        // Menu 5 : Recherche de client (Nom, Numéro de compte, identifiant de client)
        include "../v2/clients/RechercheClient.php";
    } elseif ($choixMenu == 6) {

        // Menu 6 : Afficher la liste des comptes d’un client (identifiant client)
        include "../v2/clients/affichageClientID.php";
    } elseif ($choixMenu == 7) {

        // Menu 7 : 7-	Imprimer les infos client (identifiant client)
        include "../v2/impression/infos_clients.php";
    } elseif ($choixMenu == 8) {

    // Menu 7 : 7-	Imprimer les infos client (identifiant client)
    include "../v2/comptes/SimulationFraisGestionComptes.php";
    }
}
