<?php

$fraisTenueCompte = 25;
$trouve = 0;
while (true) {

    if (!isset($comptes) || empty($comptes)) {
        change_color("red");
        echo ("Aucun compte existant!" . PHP_EOL);
        $reponse = strtoupper(readline("appuyer sur une touche pour revenir au menu et selectionner 3 "));
        change_color("");
        break;
    }

    echo ("compte(s) disponibles : " . PHP_EOL . PHP_EOL);
    foreach ($comptes as $keys => $compte) {
        foreach ($compte as $key => $value) {
            if ($key == "idCompte") {
                echo (" - " . $value . PHP_EOL);
            }
        }
    }
    echo(PHP_EOL);
    $compteRecherche = (int)readline("Saisir le numero de compte : ");
    while ($compteRecherche == "") {
        change_color(("red"));
        $compteRecherche = readline("Invalide! Veuillez Saisir le numéro de compte pour demarrer la simulation : ");
        change_color("");
    }
    while (true) {
        foreach ($comptes as $keys => $compte) {
            foreach ($compte as $key => $value) {
                if ($compteRecherche == $value) {
                    $codeClient = $compte["codeClient"];
                    $trouve = 1;
                    break 3;
                }
            }
        }
        if ($trouve != 1) {
            change_color("red");
            readline("Aucun compte trouvé avec ce numéro de compte ! appuyer sur une touvhe pour continuer");
            change_color("");
            $trouve = 0;
            break;
        }
    }

    if (isset($codeClient)) {
        while (true) {
            foreach ($clients as $cles => $client) {
                foreach ($client as $cle => $val) {
                    if ($cle == "code" && $val == $codeClient) {
                        break 3;
                    }
                }
            }
        }
    }

    $duree = (int)readline("Saisir le nombre d'années : ");
    while($duree == "" || !is_numeric($duree)) {
        change_color("red");
        $duree = (int)readline("Invalide! Saisir le nombre d'années : ");
        change_color("");
    }
    
    change_color("blue");
    echo (PHP_EOL . PHP_EOL .
        "Numéro client : " . $client["code"] . PHP_EOL .
        "Nom : " . $client["nom"] . PHP_EOL .
        "Prénom : " . $client["prenom"] . PHP_EOL .
        "Date de naissance : " . $client["dateDeNaissance"] . PHP_EOL . PHP_EOL);
    change_color("");

    if (isset($codeClient)) {
        while (true) {
            foreach ($comptes as $cles => $compte) {
                foreach ($compte as $cle => $val) {
                    if ($cle == "codeClient" && $val == $codeClient) {
                        if (isset($compte["solde"]) && $compte["solde"] >= 0 && $compte["solde"] != "") {
                            change_color("green");
                            echo("Compte courant : " . $compte["idCompte"] . PHP_EOL);
                            echo (" solde actuel : " . (int)$compte["solde"] . PHP_EOL);
                            echo (" solde après " . $duree . " an(s) : " . ((int)$compte["solde"] - ($duree * $fraisTenueCompte)) . PHP_EOL);
                            change_color("");
                        }
                        if (isset($compte["soldeLivretA"]) && (int)$compte["soldeLivretA"] >= 0 && $compte["soldeLivretA"] != "") {
                            change_color("blue");
                            echo("Livret A : " . $compte["idCompte"] . PHP_EOL);
                            echo (" solde actuel : " . (int)$compte["soldeLivretA"] . PHP_EOL);
                            echo (" solde après " . $duree . " an(s) : " . ((int)$compte["soldeLivretA"] - ($duree * $fraisTenueCompte) - ($compte["soldeLivretA"] * 0.1)) . PHP_EOL);
                            change_color("");
                        }
                        if (isset($compte["soldePel"]) && (int)$compte["soldePel"] >= 0 && $compte["soldePel"] != "") {
                            change_color("purple");
                            echo("Plan épargne logement : " . $compte["idCompte"] . PHP_EOL);
                            echo (" solde actuel : " . (int)$compte["soldePel"] . PHP_EOL);
                            echo (" solde après " . $duree . " an(s) : " . ((int)$compte["soldePel"] - ($duree * $fraisTenueCompte) - ($compte["soldePel"] * 0.025)) . PHP_EOL);
                            change_color("");
                        }
                    }
                }
            }
            break;
        }
        break;
    }
    echo(PHP_EOL);
}