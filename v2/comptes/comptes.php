<?php

// --- fichier pour la gestion des comptes --- //

while (true) {
    if (!isset($comptes)) {
        $comptes = [];
    }
    if (!isset($compte)) {
        $compte = array("codeAgence" => "", "codeClient" => "", "cptCourant" => "", "decouvert" => "", "decouvertMontant" => "", "solde" => "", "cptLivretA" => "", "soldeLivretA" => "", "cptPel" => "", "soldePEL" => "", "idCompte" => "");
    }
    if (!isset($agences) || empty($agences)) {
        change_color("red");
        echo ("Aucune agence créée !!" . PHP_EOL);
        change_color("");
        $reponse = strtoupper(readline("appuyer sur une touche pour revenir au menu et selectionner 1 "));
        break;
    }
    if (!isset($clients) || empty($clients)) {
        change_color("red");
        echo ("Aucun client créée !!" . PHP_EOL);
        change_color("");
        $reponse = strtoupper(readline("appuyer sur une touche pour revenir au menu et selectionner 2 "));
        break;
    }
    echo("Liste agences : " . PHP_EOL);
    foreach ($agences as $agence) {
        foreach ($agence as $key => $value) {
            change_color("blue");
            echo ($key . " : " . $value . " ");
            change_color("");
        }
        echo (PHP_EOL);
    }
    echo(PHP_EOL);
    $quelleAgence = (int)readline("enter le code agence : ");

    while (true) {
        foreach ($agences as $agence) {
            foreach ($agence as $key => $value) {
                if ($key == "code" && $value == $quelleAgence) {
                    $compte["codeAgence"] = $quelleAgence;
                    change_color("green");
                    echo ("Agence saisie" . PHP_EOL);
                    change_color("");
                    break 3;
                } 
            }
        }
        echo("Liste agences : " . PHP_EOL);
        foreach ($agences as $agence) {
            foreach ($agence as $key => $value) {
                change_color("blue");
                echo ($key . " : " . $value . " ");
                change_color("");
            }
            echo (PHP_EOL);
        }
        echo(PHP_EOL);

        change_color("red");
        $quelleAgence = (int)readline("Entrée invalide : enter le code agence choisi : ");
        change_color("");
    }
    echo("Liste clients : " . PHP_EOL);
    foreach ($clients as $client) {
        foreach ($client as $key => $value) {
            change_color("blue");
            echo ($key . " : " . $value . " ");
            change_color("");
        }
        echo (PHP_EOL);
    }
    echo(PHP_EOL);
    $quelClient = readline("enter le code client choisi : ");
    while (true) {

        foreach ($clients as $client) {
            foreach ($client as $key => $value) {
                if ($key == "code" && $value == $quelClient) {
                    $compte["codeClient"] = $quelClient;
                    change_color("green");
                    echo ("client saisi" . PHP_EOL);
                    change_color("");
                    break 3;
                }
            }
        }
        echo("Liste clients : " . PHP_EOL);
        foreach ($clients as $client) {
            foreach ($client as $key => $value) {
                change_color("blue");
                echo ($key . " : " . $value . " ");
                change_color("");
            }
            echo (PHP_EOL);
        }
        echo(PHP_EOL);

        change_color("red");
        $quelClient = readline("Entrée invalide : enter le code client choisi : ");
        change_color("");
    }

    while (true) {
        echo ("Veuillez sélectionner le type de compte : " . PHP_EOL . PHP_EOL .
            "------------------------------" . PHP_EOL .
            " 1. Compte courant" . PHP_EOL .
            " 2. Livret A" . PHP_EOL .
            " 3. Livret Epargne Logement" . PHP_EOL .
            "------------------------------" . PHP_EOL .
            " 4. Revenir au menu principal" . PHP_EOL . PHP_EOL);

        $choixCompte = (int)readline("Entrer votre choix : ");
        while (true) {
            if (!is_numeric($choixCompte) || $choixCompte < 1 || $choixCompte > 4) {
                change_color("red");
                $choixCompte = (int)readline("Invalide : Entrer votre choix : ");
                change_color("");
            }
            break;
        }

        if (!isset($cptCompte)) {
            $cptCompte = 0;
        }

        if ($cptCompte == 3) {
            change_color("red");
            $reponse = strtoupper(readline("Le client n° " . $quelClient . " possède déjà 3 comptes, appuyer sur touche pour revenir au menu principal"));
            change_color("");
            break 2;
        }
        if ($choixCompte == 4) {

            break;
        } elseif ($choixCompte == 1) {

            foreach ($comptes as $elements) {
                foreach ($elements as $key => $value) {
                    if ($key == "codeClient" && $value == $quelClient) {
                        if (array_key_exists("cptCourant", $elements) && $elements["cptCourant"] == 1) {
                            change_color("red");
                            $reponse = strtoupper(readline("Le client n° " . $quelClient . " possède déjà un compte courant, appuyer sur une touche"));
                            change_color("");
                            break 4;
                        }
                    }
                }
            }

            $compte["cptCourant"] = 1;
            $cptCompte++;
          
            $reponse = strtoupper(readline("Découvert autorisé (O/n) : "));
            while (true) {
                if ($reponse != "O" && $reponse != "N") {
                    change_color("red");
                    $reponse = strtoupper(readline("invalide! : Découvert autorisé (O/n) : "));
                    change_color("");
                } else {
                    break;
                }
            }

            if ($reponse == "O") {
                $compte["decouvert"] = 1;
                $reponse = (float)(readline("montant du découvert (100 <--> 1000 euros) : "));
                while (true) {
                    if (!is_numeric($reponse) || $reponse < 100 || $reponse > 1000) {
                        change_color("red");
                        $reponse = strtoupper(readline("invalide! : montant du découvert (100 <--> 1000 euros) : "));
                        change_color("");
                    } else {
                        break;
                    }
                }
                $compte["decouvertMontant"] = $reponse;
            } else {
                $compte["decouvert"] = 0;
                $compte["decouvertMontant"] = null;
            }

            $reponse = (float)(readline("solde du compte (max 10 000 euros) : "));
            if ($compte["decouvert"] == 1) {
                while (true) {
                    if (!is_numeric($reponse) || $reponse < -$compte["decouvertMontant"] || $reponse > 10000) {
                        change_color("red");
                        if ($reponse < -$compte["decouvertMontant"]) {
                            $reponse = strtoupper(readline("invalide! : découvert autorisé (" . $compte["decouvertMontant"] . " euros) dépassé. "));
                        } else {
                            $reponse = strtoupper(readline("invalide! : solde max (10 000 euros) : "));
                        }
                        change_color("");
                    } else {
                        break;
                    }
                }
                $compte["solde"] = $reponse;
            } elseif ($compte["decouvert"] == 0) {
                while (true) {
                    if (!is_numeric($reponse) || $reponse < 0 || $reponse > 10000) {
                        change_color("red");
                        $reponse = strtoupper(readline("Découvert non autorisé pour ce compte! : montant max (10 000 euros)."));
                        change_color("");
                    } else {
                        break;
                    }
                }
                $compte["solde"] = $reponse;
            }

            $reponse = readline("appuyer sur une touche pour continuer");
            break;
            
        } elseif ($choixCompte == 2) {

            foreach ($comptes as $elements) {
                foreach ($elements as $key => $value) {
                    if ($key == "codeClient" && $value == $quelClient) {
                        if (array_key_exists("cptLivretA", $elements) && $elements["cptLivretA"] == 1) {
                            change_color("red");
                            $reponse = strtoupper(readline("Le client n° " . $quelClient . " possède déjà un livret A, appuyer sur une touche"));
                            change_color("");
                            break 4;
                        }
                    }
                }
            }
            $compte["cptLivretA"] = 1;
            $cptCompte++;
            $reponse = (float)(readline("solde du livret A (max 10 000 euros) : "));
            while (true) {
                if (!is_numeric($reponse) || $reponse < 0 || $reponse > 10000) {
                    change_color("red");
                    if ($reponse < 0) {
                        $reponse = strtoupper(readline("invalide! : solde négatif impossible!" . PHP_EOL ."solde du livret A (max 10 000 euros) :  "));
                    } else {
                        $reponse = strtoupper(readline("invalide! : appuyer sur une touche : "));
                    }
                    change_color("");
                } else {
                    break;
                }
            }
            $compte["soldeLivretA"] = $reponse;
            $reponse = readline("appuyer sur une autre touche pour continuer");
            break;
            
        } elseif ($choixCompte == 3) {

            // if (!isset($compte["cptPel"])) {
            //     $compte["cptPel"] = 0;
            // }
            // if (!array_key_exists("cptPel", $compte)) {
            //     $compte["cptPel"] = 0;
            // }

            foreach ($comptes as $elements) {
                foreach ($elements as $key => $value) {
                    if ($key == "codeClient" && $value == $quelClient) {
                        if (array_key_exists("cptPel", $elements) && $elements["cptPel"] == 1) {
                            change_color("red");
                            $reponse = strtoupper(readline("Le client n° " . $quelClient . " possède déjà un compte épargne logement, appuyer sur une touche"));
                            change_color("");
                            break 4;
                        }
                    }
                }
            }
            $compte["cptPel"] = 1;
            $cptCompte++;
            $reponse = (float)(readline("solde du PEL (max 10 000 euros) : " . PHP_EOL));
            while (true) {
                if (!is_numeric($reponse) || $reponse < 0 || $reponse > 10000) {
                    change_color("red");
                    if ($reponse < 0) {
                        $reponse = strtoupper(readline("invalide! : solde négatif impossible! solde du PEL (max 10 000 euros) : "));
                    } else {
                        $reponse = strtoupper(readline("invalide! : solde du PEL (max 10 000 euros) : "));
                    }
                    change_color("");
                } else {
                    break;
                }
            }
            $compte["soldePEL"] = $reponse;
           
            $reponse = readline("appuyer sur une autre touche pour continuer");
            
            break;
        }
    }

    $numeroCompte = random_int(10000000000, 99999999999);
    foreach ($comptes as $keys => $elements) {
        foreach ($elements as $key => $value) {
            if ($key == "nom") {
                while ($numeroCompte == $value) {
                    $numeroCompte = random_int(10000000000, 99999999999);
                }
            }
        }
    }
    $compte["idCompte"] = $numeroCompte;
    change_color("green");
    echo ("Le compte n° " . $numeroCompte . " a bien été créé" . PHP_EOL);
    change_color("");
    $comptes[] = $compte;
    arrayToCsv($filename = '../v2/comptes/comptes.csv', $delimiter = ',', $comptes, $header = array("codeAgence", "codeClient", "cptCourant", "decouvert", "decouvertMontant", "solde", "cptLivretA", "soldeLivretA", "cptPel", "soldePEL", "idCompte"));

    $newCompte = strtoupper(readline("Voulez-vous créer un autre compte ? (O/N) : "));
    while ($newCompte != "O" && $newCompte != "N") {
        change_color("red");
        $newCompte = strtoupper(readline("Réponse invalide, Voulez-vous créer un autre compte ? (O/N) : "));
        change_color("");
    }
    if ($newCompte == "O") {
    continue;
    }
    break;
}
