<?php

while (true) {

    echo ("Identifiant(s) disponible(s) : " . PHP_EOL);
    foreach ($clients as $keys => $client) {
        foreach ($client as $key => $value) {
            if ($key == "code") {
                echo ("Nom : " . $client["nom"] . " => identifiant : " . $value . PHP_EOL);
            }
        }
    }
    $clientRecherche = readline("Saisir l'identifiant du client à afficher : ");
    while ($clientRecherche == "") {
        change_color(("red"));
        $clientRecherche = readline("Invalide! Veuillez Saisir l'identifiant client choisi");
        change_color("");
    }
    while (true) {
        foreach ($comptes as $keys => $compte) {
            foreach ($compte as $key => $value) {
                if ($clientRecherche == $value) {
                    $codeClient = $compte["codeClient"];
                    break 3;
                }
            }
        }
        change_color("red");
        readline("Aucun client trouvé avec ce numéro d'identifiant' ! appuyer sur entrer : ");
        change_color("");
        break;
    }

    if (isset($codeClient)) {
        while (true) {
            foreach ($clients as $cles => $client) {
                foreach ($client as $cle => $val) {
                    if ($cle == "code" && $val == $codeClient) {
                        break 3;
                    }
                }
            }
        }
    }
    

    $afficherFicheClient = ("../v1/impression/Infosclient.txt");
    $f = fopen($afficherFicheClient, 'w');

    $ficheClient = ("                      Fiche client" . PHP_EOL . PHP_EOL .
        "Numéro client : " . $client["code"] . PHP_EOL .
        "Nom : " . $client["nom"] . PHP_EOL .
        "Prénom : " . $client["prenom"] . PHP_EOL .
        "Date de naissance : " . $client["dateDeNaissance"] . PHP_EOL . PHP_EOL .
        "_____________________________________________________________________" . PHP_EOL .
        "Liste de compte" . PHP_EOL .
        "_____________________________________________________________________" . PHP_EOL .
        "Numéro de compte                  Solde" . PHP_EOL .
        "_____________________________________________________________________" . PHP_EOL . PHP_EOL);

    fwrite($f, $ficheClient);
    if (isset($codeClient)) {
        while (true) {
            foreach ($comptes as $cles => $compte) {
                foreach ($compte as $cle => $val) {
                    if ($cle == "codeClient" && $val == $codeClient) {
                        if (isset($compte["solde"]) && $compte["solde"] >= 0) {
                            $courant = ("    " . $compte["idCompte"] . "                   " . $compte["solde"] . "                   :-)" . PHP_EOL);
                            fwrite($f, $courant);
                        } elseif (isset($compte["solde"]) && $compte["solde"] <= 0) {
                            $courant = ("    " . $compte["idCompte"] . "                   " . $compte["solde"] . "                   :-(" . PHP_EOL);
                            fwrite($f, $courant);
                        }
                        if (isset($compte["soldeLivretA"]) && $compte["soldeLivretA"] >= 0) {
                            $livretA = ("    " . $compte["idCompte"] . "                   " . $compte["soldeLivretA"] . "                   :-)" . PHP_EOL);
                            fwrite($f, $livretA);
                        } elseif (isset($compte["soldeLivretA"]) && $compte["soldeLivretA"] <= 0) {
                            $livretA = ("    " . $compte["idCompte"] . "                   " . $compte["soldeLivretA"] . "                   :-(" . PHP_EOL);
                            fwrite($f, $livretA);
                        }
                        if (isset($compte["soldePEL"]) && $compte["soldePEL"] >= 0) {
                            $pel = ("    " . $compte["idCompte"] . "                   " . $compte["soldePEL"] . "                   :-)" . PHP_EOL);
                            fwrite($f, $pel);
                        }
                        if (isset($compte["soldePEL"]) && $compte["soldePEL"] <= 0) {
                            $pel = ("    " . $compte["idCompte"] . "                   " . $compte["soldePEL"] . "                   :-(" . PHP_EOL);
                            fwrite($f, $pel);
                        }
                    }
                }
            }
            break;
        }
        // $afficherFicheClient = ("../v1/impression/Infosclient.txt");
        // $f = fopen($afficherFicheClient, 'w');
        // fwrite($f, $ficheClient);
        // fwrite($f, $courant);
        // fwrite($f, $livretA);
        // fwrite($f, $pel);
        fclose($f);
        // $f = fopen($afficherFicheClient, 'r');
        change_color("green");
        readline("Le fichier 'client' est prêt pour l'impression.");
        change_color("");
        break;
    }
}
