<?php

while (true) {

    if (!isset($comptes) || empty($comptes)) {
        change_color("red");
        echo ("Aucun compte existant!" . PHP_EOL);
        $reponse = strtoupper(readline("appuyer sur une touche pour revenir au menu et selectionner 3 "));
        change_color("");
        break;
    }

    echo ("compte(s) disponibles : " . PHP_EOL . PHP_EOL);
    foreach ($comptes as $keys => $compte) {
        foreach ($compte as $key => $value) {
            if ($key == "idCompte") {
                echo ("Nom : " . $client["nom"] . " => compte n° " . $value .  PHP_EOL);
            }
        }
    }
    echo(PHP_EOL);
    $compteRecherche = (int)readline("Saisir le numero de compte : ");
    while ($compteRecherche == "") {
        change_color(("red"));
        $compteRecherche = readline("Invalide! Veuillez Saisir le numéro de compte du client à afficher : ");
        change_color("");
    }
    while (true) {
        foreach ($comptes as $keys => $compte) {
            foreach ($compte as $key => $value) {
                if ($compteRecherche == $value) {
                    $codeClient = $compte["codeClient"];
                    $trouve = 1;
                    break 3;
                }
            }
        }
        if ($trouve != 1) {
            change_color("red");
            readline("Aucun compte trouvé avec ce nom ! appuyer sur une touvhe pour continuer");
            change_color("");
            $trouve = 0;
            break;
        }
    }

    if (isset($codeClient)) {
        while (true) {
            foreach ($clients as $cles => $client) {
                foreach ($client as $cle => $val) {
                    if ($cle == "code" && $val == $codeClient) {
                        break 3;
                    }
                }
            }
        }
    }

    change_color("blue");
    echo (PHP_EOL . PHP_EOL .
        "                      Fiche client" . PHP_EOL . PHP_EOL .
        "Numéro client : " . $client["code"] . PHP_EOL .
        "Nom : " . $client["nom"] . PHP_EOL .
        "Prénom : " . $client["prenom"] . PHP_EOL .
        "Date de naissance : " . $client["dateDeNaissance"] . PHP_EOL . PHP_EOL .
        "_____________________________________________________________________" . PHP_EOL .
        "Liste de compte" . PHP_EOL .
        "_____________________________________________________________________" . PHP_EOL .
        "Numéro de compte                   Solde" . PHP_EOL .
        "_____________________________________________________________________" . PHP_EOL . PHP_EOL);
    change_color("");

    if (isset($codeClient)) {
        while (true) {
            foreach ($comptes as $cles => $compte) {
                foreach ($compte as $cle => $val) {
                    if ($cle == "codeClient" && $val == $codeClient) {
                        if (isset($compte["solde"]) && $compte["solde"] >= 0) {
                            change_color("green");
                            echo ("    " . $compte["idCompte"] . "                    " . $compte["solde"] . "                    :-)" . PHP_EOL);
                            change_color("");
                        } elseif (isset($compte["solde"]) && $compte["solde"] <= 0) {
                            change_color("red");
                            echo ("    " . $compte["idCompte"] . "                    " . $compte["solde"] . "                    :-(" . PHP_EOL);
                            change_color("");
                        }
                        if (isset($compte["soldeLivretA"]) && $compte["soldeLivretA"] >= 0) {
                            change_color("green");
                            echo ("    " . $compte["idCompte"] . "                    " . $compte["soldeLivretA"] . "                    :-)" . PHP_EOL);
                            change_color("");
                        } elseif (isset($compte["soldeLivretA"]) && $compte["soldeLivretA"] <= 0) {
                            change_color("red");
                            echo ("    " . $compte["idCompte"] . "                    " . $compte["soldeLivretA"] . "                    :-(" . PHP_EOL);
                            change_color("");
                        }
                        if (isset($compte["soldePEL"]) && $compte["soldePEL"] >= 0) {
                            change_color("green");
                            echo ("    " . $compte["idCompte"] . "                    " . $compte["soldePEL"] . "                    :-)" . PHP_EOL);
                            change_color("");
                        }
                        if (isset($compte["soldePEL"]) && $compte["soldePEL"] <= 0) {
                            change_color("red");
                            echo ("    " . $compte["idCompte"] . "                    " . $compte["soldePEL"] . "                    :-(" . PHP_EOL);
                            change_color("");
                        }
                    }
                }
            }
            break;
        }
        break;
    }
    echo(PHP_EOL);
}
