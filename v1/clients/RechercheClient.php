<?php

while (true) {

    if (!isset($comptes) || empty($comptes)) {
        echo ("Aucun compte existant!" . PHP_EOL);
        $reponse = strtoupper(readline("appuyer sur une touche pour revenir au menu et selectionner 3 "));
        break;
    }

    echo ("Rechercher par : " . PHP_EOL . PHP_EOL .
        "--------------------------" . PHP_EOL .
        " 1. Nom" . PHP_EOL .
        " 2. Numéro de compte" . PHP_EOL .
        " 3. identifiant de client" . PHP_EOL .
        "--------------------------" . PHP_EOL .
        " 4. Quitter" . PHP_EOL . PHP_EOL);

    $choixRecherche = (int)readline("Entrer votre choix : ");
    while (true) {
        if (!is_numeric($choixCompte) || $choixCompte < 1 || $choixCompte > 3 || $choixCompte == "") {
            change_color("red");
            $choixCompte = (int)readline("Invalide : Entrer votre choix : ");
            change_color("");
        }
        break;
    }

    if ($choixRecherche == 4) {
        break;
    } elseif ($choixRecherche == 1) {

        while (true) {

            echo ("noms disponibles : " . PHP_EOL . PHP_EOL);
            foreach ($clients as $keys => $client) {
                foreach ($client as $key => $value) {
                    if ($key == "nom") {
                        echo ("Nom : " . $value . " => identifiant : " . $client["code"] . PHP_EOL);
                    }
                }
            }
            echo(PHP_EOL);
            $clientRecherche = readline("Saisir le nom du client à afficher : ");
            change_color("red");
            while ($clientRecherche == "") {
                $clientRecherche = readline("Invalide! Veuillez Saisir le nom du client à afficher");
            }
            change_color("");
            while (true) {
                foreach ($clients as $keys => $client) {
                    foreach ($client as $key => $value) {
                        if ($clientRecherche == $value) {
                            $codeClient = $client["code"];
                            $trouve = 1;
                            break 3;
                        }
                    }
                }
                if ($trouve != 1) {
                    change_color("red");
                    readline("Aucun client trouvé avec ce numéro de compte ! appuyer sur une touche pour continuer ");
                    change_color("");
                    $trouve = 0;
                    break;
                } 
            }
            change_color("blue");
            echo (PHP_EOL . PHP_EOL .
                "                      Fiche client" . PHP_EOL . PHP_EOL .
                "Numéro client : " . $client["code"] . PHP_EOL .
                "Nom : " . $client["nom"] . PHP_EOL .
                "Prénom : " . $client["prenom"] . PHP_EOL .
                "Date de naissance : " . $client["dateDeNaissance"] . PHP_EOL . PHP_EOL .
                "_____________________________________________________________________" . PHP_EOL .
                "Liste de compte" . PHP_EOL .
                "_____________________________________________________________________" . PHP_EOL .
                "Numéro de compte                   Solde" . PHP_EOL .
                "_____________________________________________________________________" . PHP_EOL . PHP_EOL);
            change_color("");
        
            if (isset($codeClient)) {
                while (true) {
                    foreach ($comptes as $cles => $compte) {
                        foreach ($compte as $cle => $val) {
                            if ($cle == "codeClient" && $val == $codeClient) {
                                if (isset($compte["solde"]) && $compte["solde"] >= 0) {
                                    change_color("green");
                                    echo ("    " . $compte["idCompte"] . "                    " . $compte["solde"] . "                    :-)" . PHP_EOL);
                                    change_color("");
                                } elseif (isset($compte["solde"]) && $compte["solde"] <= 0) {
                                    change_color("red");
                                    echo ("    " . $compte["idCompte"] . "                    " . $compte["solde"] . "                    :-(" . PHP_EOL);
                                    change_color("");
                                }
                                if (isset($compte["soldeLivretA"]) && $compte["soldeLivretA"] >= 0) {
                                    change_color("green");
                                    echo ("    " . $compte["idCompte"] . "                    " . $compte["soldeLivretA"] . "                    :-)" . PHP_EOL);
                                    change_color("");
                                } elseif (isset($compte["soldeLivretA"]) && $compte["soldeLivretA"] <= 0) {
                                    change_color("red");
                                    echo ("    " . $compte["idCompte"] . "                    " . $compte["soldeLivretA"] . "                    :-(" . PHP_EOL);
                                    change_color("");
                                }
                                if (isset($compte["soldePEL"]) && $compte["soldePEL"] >= 0) {
                                    change_color("green");
                                    echo ("    " . $compte["idCompte"] . "                    " . $compte["soldePEL"] . "                    :-)" . PHP_EOL);
                                    change_color("");
                                }
                                if (isset($compte["soldePEL"]) && $compte["soldePEL"] <= 0) {
                                    change_color("red");
                                    echo ("    " . $compte["idCompte"] . "                    " . $compte["soldePEL"] . "                    :-(" . PHP_EOL);
                                    change_color("");
                                }
                            }
                        }
                    }
                    break;
                }
                break;
            }
            echo(PHP_EOL);
        }
    } elseif ($choixRecherche == 2) {

        while (true) {


            echo ("Numéro(s) de compte disponible(s) : " . PHP_EOL . PHP_EOL);
            foreach ($comptes as $keys => $compte) {
                foreach ($compte as $key => $value) {
                    if ($key == "idCompte") {
                        echo ("Nom : " . $client["nom"] . " => compte n° " . $value .  PHP_EOL);
                    }
                }
            }
            echo(PHP_EOL);
            $clientRecherche = (int)readline("Saisir le numéro de compte du client à afficher : ");
            change_color("red");
            while ($clientRecherche == "") {
                $clientRecherche = (int)readline("Invalide! Saisir le numéro de compte du client à afficher : ");
            }
            change_color("");

            while (true) {
                foreach ($comptes as $keys => $compte) {
                    foreach ($compte as $key => $value) {
                        if ($clientRecherche == $value) {
                            $codeClient = $compte["codeClient"];
                            $trouve = 1;
                            break 3;
                        }
                    }
                }
                if ($trouve != 1) {
                    change_color("red");
                    readline("Aucun client trouvé avec ce numéro de compte ! appuyer sur une touche pour continuer ");
                    change_color("");
                    $trouve = 0;
                    break;
                }
            }

            change_color("blue");
            echo (PHP_EOL . PHP_EOL .
                "                      Fiche client" . PHP_EOL . PHP_EOL .
                "Numéro client : " . $client["code"] . PHP_EOL .
                "Nom : " . $client["nom"] . PHP_EOL .
                "Prénom : " . $client["prenom"] . PHP_EOL .
                "Date de naissance : " . $client["dateDeNaissance"] . PHP_EOL . PHP_EOL .
                "_____________________________________________________________________" . PHP_EOL .
                "Liste de compte" . PHP_EOL .
                "_____________________________________________________________________" . PHP_EOL .
                "Numéro de compte                   Solde" . PHP_EOL .
                "_____________________________________________________________________" . PHP_EOL . PHP_EOL);
            change_color("");

            if (isset($codeClient)) {
                while (true) {
                    foreach ($comptes as $cles => $compte) {
                        foreach ($compte as $cle => $val) {
                            if ($cle == "codeClient" && $val == $codeClient) {
                                if (isset($compte["solde"]) && $compte["solde"] >= 0) {
                                    change_color("green");
                                    echo ("    " . $compte["idCompte"] . "                    " . $compte["solde"] . "                    :-)" . PHP_EOL);
                                    change_color("");
                                } elseif (isset($compte["solde"]) && $compte["solde"] <= 0) {
                                    change_color("red");
                                    echo ("    " . $compte["idCompte"] . "                    " . $compte["solde"] . "                    :-(" . PHP_EOL);
                                    change_color("");
                                }
                                if (isset($compte["soldeLivretA"]) && $compte["soldeLivretA"] >= 0) {
                                    change_color("green");
                                    echo ("    " . $compte["idCompte"] . "                    " . $compte["soldeLivretA"] . "                    :-)" . PHP_EOL);
                                    change_color("");
                                } elseif (isset($compte["soldeLivretA"]) && $compte["soldeLivretA"] <= 0) {
                                    change_color("red");
                                    echo ("    " . $compte["idCompte"] . "                    " . $compte["soldeLivretA"] . "                    :-(" . PHP_EOL);
                                    change_color("");
                                }
                                if (isset($compte["soldePEL"]) && $compte["soldePEL"] >= 0) {
                                    change_color("green");
                                    echo ("    " . $compte["idCompte"] . "                    " . $compte["soldePEL"] . "                    :-)" . PHP_EOL);
                                    change_color("");
                                }
                                if (isset($compte["soldePEL"]) && $compte["soldePEL"] <= 0) {
                                    change_color("red");
                                    echo ("    " . $compte["idCompte"] . "                    " . $compte["soldePEL"] . "                    :-(" . PHP_EOL);
                                    change_color("");
                                }
                            }
                        }
                    }
                    break;
                }
                break;
            }
            echo(PHP_EOL);
        }
    } elseif ($choixRecherche == 3) {

        while (true) {

            echo ("Identifiant(s) disponible(s) : " . PHP_EOL . PHP_EOL);
            foreach ($clients as $keys => $client) {
                foreach ($client as $key => $value) {
                    if ($key == "code") {
                        echo ("Nom : " . $client["nom"] . " => identifiant : " . $value . PHP_EOL);
                    }
                }
            }
            echo(PHP_EOL);
            $clientRecherche = readline("Saisir l'identifiant du client à afficher : ");
            while ($clientRecherche == "") {
                change_color(("red"));
                $clientRecherche = readline("Invalide! Veuillez Saisir l'identifiant du client à afficher : ");
                change_color("");
            }
            while (true) {
                foreach ($comptes as $keys => $compte) {
                    foreach ($compte as $key => $value) {
                        if ($clientRecherche == $value) {
                            $codeClient = $compte["codeClient"];
                            $trouve = 1;
                            break 3;
                        }
                    }
                }
                if ($trouve != 1) {
                    change_color("red");
                    readline("Aucun client trouvé avec ce numéro d'identifiant' ! appuyer sur une touche pour continuer ");
                    change_color("");
                    $trouve = 0;
                    break;
                }
            }

            // if (isset($codeClient)) {
            //     while (true) {
            //         foreach ($clients as $cles => $client) {
            //             foreach ($client as $cle => $val) {
            //                 if ($cle == "code" && $val == $codeClient) {
            //                     break 3;
            //                 }
            //             }
            //         }
            //     }
            // }

            change_color("blue");
            echo (PHP_EOL . PHP_EOL .
                "                      Fiche client" . PHP_EOL . PHP_EOL .
                "Numéro client : " . $client["code"] . PHP_EOL .
                "Nom : " . $client["nom"] . PHP_EOL .
                "Prénom : " . $client["prenom"] . PHP_EOL .
                "Date de naissance : " . $client["dateDeNaissance"] . PHP_EOL . PHP_EOL .
                "_____________________________________________________________________" . PHP_EOL .
                "Liste de compte" . PHP_EOL .
                "_____________________________________________________________________" . PHP_EOL .
                "Numéro de compte                   Solde" . PHP_EOL .
                "_____________________________________________________________________" . PHP_EOL . PHP_EOL);
            change_color("");

            if (isset($codeClient)) {
                while (true) {
                    foreach ($comptes as $cles => $compte) {
                        foreach ($compte as $cle => $val) {
                            if ($cle == "codeClient" && $val == $codeClient) {
                                if (isset($compte["solde"]) && $compte["solde"] >= 0) {
                                    change_color("green");
                                    echo ("    " . $compte["idCompte"] . "                    " . $compte["solde"] . "                    :-)" . PHP_EOL);
                                    change_color("");
                                } elseif (isset($compte["solde"]) && $compte["solde"] <= 0) {
                                    change_color("red");
                                    echo ("    " . $compte["idCompte"] . "                    " . $compte["solde"] . "                    :-(" . PHP_EOL);
                                    change_color("");
                                }
                                if (isset($compte["soldeLivretA"]) && $compte["soldeLivretA"] >= 0) {
                                    change_color("green");
                                    echo ("    " . $compte["idCompte"] . "                    " . $compte["soldeLivretA"] . "                    :-)" . PHP_EOL);
                                    change_color("");
                                } elseif (isset($compte["soldeLivretA"]) && $compte["soldeLivretA"] <= 0) {
                                    change_color("red");
                                    echo ("    " . $compte["idCompte"] . "                    " . $compte["soldeLivretA"] . "                    :-(" . PHP_EOL);
                                    change_color("");
                                }
                                if (isset($compte["soldePEL"]) && $compte["soldePEL"] >= 0) {
                                    change_color("green");
                                    echo ("    " . $compte["idCompte"] . "                    " . $compte["soldePEL"] . "                    :-)" . PHP_EOL);
                                    change_color("");
                                }
                                if (isset($compte["soldePEL"]) && $compte["soldePEL"] <= 0) {
                                    change_color("red");
                                    echo ("    " . $compte["idCompte"] . "                    " . $compte["soldePEL"] . "                    :-(" . PHP_EOL);
                                    change_color("");
                                }
                            }
                        }
                    }
                    break;
                }
                break;
            }
        }
    }
    echo(PHP_EOL);
}
