<?php

// fichier pour la gestion des agences

while (true) {
    if (!isset($agences)) {
        $agences = [];
    }
    $idAgence = random_int(100, 999);
    foreach ($agences as $elements) {
        foreach ($elements as $key => $value) {
            if ($key == "code") {
                while ($idAgence == $value) {
                    $idAgence = random_int(100, 999);
                }
            }
        }
    }

    $nomAgence = readline("Entrer le nom de l'agence : ");

    while (!preg_match("/[a-zA-Z][\p{L}-]*$/", $nomAgence)) {
        change_color("red");
        echo ("Nom invalide !" . PHP_EOL);
        change_color("");
        $nomAgence = readline("Entrer le nom de l'agence : ");
    }

    foreach ($agences as $keys => $elements) {
        foreach ($elements as $key => $value) {
            if ($key == "nom") {
                while ($nomAgence == $value) {
                    change_color("red");
                    echo ("Une entrée proche a été trouvée : " . PHP_EOL . PHP_EOL);
                    change_color("");
                    change_color("purple");
                    echo("    Agence : " . $agence["nom"] . PHP_EOL .
                    "    ID : " . $agence["code"] . PHP_EOL . 
                    "    Adresse : " . $agence["adresse"] . PHP_EOL . PHP_EOL);
                    change_color("");
                    $reponse = strtoupper(readline("Voulez-vous continuer (O)ui / (N)on ? "));
                    while ($reponse != "O" && $reponse != "N") {
                        change_color("red");
                        $reponse = strtoupper(readline("Réponse invalide, taper (O)ui / (N)on : "));
                        change_color("");
                    }
                    if ($reponse == "O") {
                        $agence["nom"] = $nomAgence;
                        break 3;
                    } elseif ($reponse == "N") {
                        break 4;
                    }
                }
            }
        }
    }
            
    $rue = readline("Renseigner la rue (obligatoire) : ");
    while ($rue == "" || is_numeric($rue)) {
        change_color("red");
        echo ("Rue ou lieu-dit (obligatoire!)" . PHP_EOL);
        change_color("");
        $rue = readline("Renseigner la rue : ");
    }
   
    $codePostal = readline("entrer le code postal (obligatoire) : ");
    while (!preg_match("~^[0-9]{5}$~", $codePostal)) {
        change_color("red");
        echo ("Code postal invalide !" . PHP_EOL);
        change_color("");
        $codePostal = readline("Entrer le code postal : ");
    }
   
    $ville = readline("entrer la ville : ");
    while ($ville == "" || is_numeric($ville)) {
        change_color("red");
        echo ("ville invalide!" . PHP_EOL);
        change_color("");
        $ville = readline("Entrer la ville : ");
    }

    $agenceAdresse = $rue . " " . $codePostal . " " . $ville;

    foreach ($agences as $keys => $elements) {
        foreach ($elements as $key => $value) {
            if ($key == "adresse") {
                while ($agenceAdresse == $value) {
                    echo ("Une entrée proche a été trouvée : " . PHP_EOL . PHP_EOL);
                    change_color("purple");
                    echo("    Agence : " . $agence["nom"] . PHP_EOL .
                    "    ID : " . $agence["code"] . PHP_EOL . 
                    "    Adresse : " . $agence["adresse"] . PHP_EOL . PHP_EOL);
                    change_color("");
                    $reponse = strtoupper(readline("Voulez-vous continuer (O)ui / (N)on ? "));
                    while ($reponse != "O" && $reponse != "N") {
                        change_color("red");
                        $reponse = strtoupper(readline("Réponse invalide, taper (O)ui / (N)on : "));
                        change_color("");
                    }
                    if ($reponse == "O") {
                        $agence["adresse"] = $nomAgence;
                        break 3;
                    } elseif ($reponse == "N") {
                        break 4;
                    }
                }
            }
        }
    }
    $agence["code"] = $idAgence;
    $agence["nom"] = $nomAgence;
    $agence["adresse"] = $agenceAdresse;
    $agences[] = $agence;
    change_color("green");
    $newAgence = strtoupper(readline("Voulez-vous créer une autre agence ? (O)ui / (N)on : "));
    change_color("");
    if ($newAgence == "O") {
        continue;
    }
    break;
}