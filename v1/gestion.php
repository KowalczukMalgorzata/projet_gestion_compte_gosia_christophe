<?php

// fichier principal pour la gestion des comptes banquaires

// fichier utils 

include "../v1/lib/utils.php";

// fonctions
// include "../projet_gestion_compte_gosia_christophe/lib/function.php";

// $client["nbreCompte"] = 0;

while (true) {

    // Menu Principal pour le programme de gestion de comptes bancaires

    change_color("blue");
    echo (PHP_EOL . "Menu :" . PHP_EOL . PHP_EOL);
    change_color("");
    change_color("purple");
    echo ("   ---------------------- DWWM_20044 Banque ----------------------------" . PHP_EOL .
        "   1- Créer une agence" . PHP_EOL .
        "   2- créer un client" . PHP_EOL .
        "   3- Créer un compte bancaire" . PHP_EOL .
        "   4- Recherche de compte (numéro de compte)" . PHP_EOL .
        "   5- Recherche de client (Nom, Numéro de compte, identifiant de client)" . PHP_EOL .
        "   6- Afficher la liste des comptes d’un client (identifiant client)" . PHP_EOL .
        "   7- Imprimer les infos client (identifiant client)" . PHP_EOL .
        "   ---------------------------------------------------------------------" . PHP_EOL .
        "   8- Quitter le programme" . PHP_EOL .
        "   ---------------------------------------------------------------------" . PHP_EOL . PHP_EOL);
    change_color("");
    change_color("blue");
    $choixMenu = (int)(readline("Votre choix : "));
    change_color("");
    echo(PHP_EOL);
    while (true) {
        if (!is_numeric($choixMenu) || $choixMenu < 1 || $choixMenu > 8) {
            change_color("red");
            $choixMenu = (int)(readline("choix  invalide : "));
            change_color("");
        } else {
            break;
        }
    }

    if ($choixMenu == 8) {

        break;
    } elseif ($choixMenu == 1) {

        // Menu 1 : Créer une agence
        include "../v1/agences/agences.php";

    } elseif ($choixMenu == 2) {

        // Menu 2 : Créer un client
       include "../v1/clients/clients.php";

    } elseif ($choixMenu == 3) {

        // Menu 3 : Créer un compte bancaire
        include "../v1/comptes/comptes.php";
        
    } elseif ($choixMenu == 4) {

        // Menu 4 : Recherche de compte (numéro de compte)
        include "../v1/comptes/rechercheCompte.php";
        
    } elseif ($choixMenu == 5) {

        // Menu 5 : Recherche de client (Nom, Numéro de compte, identifiant de client)
        include "../v1/clients/RechercheClient.php";
        
    } elseif ($choixMenu == 6) {

        // Menu 6 : Afficher la liste des comptes d’un client (identifiant client)
        include "../v1/clients/affichageClientID.php";

    } elseif ($choixMenu == 7) {

        // Menu 7 : 7-	Imprimer les infos client (identifiant client)
        include "../v1/impression/infos_clients.php";

    }
}
